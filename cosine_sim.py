import numpy as np
from numpy.linalg import norm

class cosine_sim:

    def cosine_sim_cal(self, a, b):
        if norm(a)==0 or norm(b)==0:
            return 0
        cos_sim =np.dot(a, b)/(norm(a)*norm(b))
        return cos_sim

            
        
if __name__ == '__main__' :
    # test = cosine_sim()
    # print(test.cosine_sim_cal('고수고수고고수'))
    print(cosine_sim().cosine_sim_cal([1,1,1],[0,0,0]))

