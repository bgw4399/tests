import pickle
from gensim.models.word2vec import Word2Vec
from cosine_sim import cosine_sim

class Word2vec_movie:

    def __init__(self):

        self.model_word2vec = Word2Vec.load("word2vec_final")
        
        # DB에서 가져와야됨
        with open('data/processed_daum_movie_final.pickle', 'rb') as f:
            data = pickle.load(f)

        self.title = [i['title'] for i in data]
        self.story = [i['story'] for i in data]


    def cosine_sim_calc(self, movie_title):
        
        movie_title_vector = 0

        if movie_title in self.title:
            for word in self.story[self.title.index(movie_title)]:
                if word in self.model_word2vec.wv:
                    movie_title_vector += self.model_word2vec.wv[word]
            #print(movie_title_vector)
            sim_movie = []
            for index in range(len(self.story)):
                if not self.story[index] :
                    continue
                # print(self.story[index])
                movie_diff_vector = 0
                for word in self.story[index]:
                    if word in self.model_word2vec.wv:
                        movie_diff_vector += self.model_word2vec.wv[word]
                #print(cosine_sim().cosine_sim_cal(movie_title_vector, movie_diff_vector))
                cosine_sim_value = cosine_sim().cosine_sim_cal(movie_title_vector, movie_diff_vector)
                # print(cosine_sim_value)
                if cosine_sim_value > 0.5 :
                    tmp = []
                    tmp.append(float(cosine_sim_value))
                    tmp.append(self.title[index])
                    sim_movie.append(tmp)
            return sorted(sim_movie, key=lambda x:-x[0])[1:8]



                

if __name__ == '__main__' :
    movie = Word2vec_movie()
    print(len(movie.title))
    print(len(movie.story))
    for i, j in zip(movie.title, movie.story):
        print(i, j)
    print(movie.title.index("판문점"))
    print(movie.cosine_sim_calc("극장판 포켓몬스터 모두의 이야기"))
